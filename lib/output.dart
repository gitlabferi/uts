import 'package:flutter/material.dart';
import './control.dart';

class Output extends StatefulWidget{
  @override
  _OutputState createState() => _OutputState();
}

class _OutputState extends State<Output>{
  String msg = 'kate Meralda';

  void _changeText() {
    setState(() {
      if (msg.startsWith('S')) {
        msg = 'Sonny';
      } else if (msg.startsWith('M')) {
        msg = 'Meralda';
      }else{
        msg = 'kate';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('My Name Is : ', style: new TextStyle(fontSize:30.0),),
          Control(msg),
          RaisedButton(child: Text("Change Name",style: new TextStyle( color: Colors.black),),color: Colors.blueGrey,onPressed:_changeText,),
        ],
      ),
    );
  }
}