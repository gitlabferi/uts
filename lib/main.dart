import 'package:flutter/material.dart';
import './output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS MOBILE APP',
      home: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          leading: new Icon(Icons.home),
          title: Text('UTS MOBILE APP'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: Output(),
      ),
    );
  }
}